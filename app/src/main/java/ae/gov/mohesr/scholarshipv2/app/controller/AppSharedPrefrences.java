package ae.gov.mohesr.scholarshipv2.app.controller;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hussamj on 10/26/15.
 */
public class AppSharedPrefrences {
    public static final String NO_DATA_SAVED = "NoDataSaved";
    protected static AppSharedPrefrences instance = null;

    public static synchronized AppSharedPrefrences getInstance() {
        if (instance == null) {
            instance = new AppSharedPrefrences();
        }
        return instance;
    }

    public void sharedPreferancesCreator(Context context, String key,
                                         String value) {
        SharedPreferences myPrefs = context.getSharedPreferences("myPrefs",
                context.MODE_WORLD_READABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(key, value);

        prefsEditor.commit();
    }

    public String getSharedPreferances(Context context, String key) {
        String result = "";
        SharedPreferences myPrefs = context.getSharedPreferences("myPrefs",
                context.MODE_WORLD_READABLE);
        result = myPrefs.getString(key, NO_DATA_SAVED);
        return result;
    }
}
