package ae.gov.mohesr.scholarshipv2.main;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IntRange;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import ae.gov.mohesr.scholarshipv2.R;
import ae.gov.mohesr.scholarshipv2.kbv.KenBurnsView;

public class SplashActivity extends Activity {
    ImageView fourSquare, img_scholaships_app_logo;
    private static int SPLASH_TIME_OUT = 3000;
    TextView textView_uae, textView_portal;
    private KenBurnsView mKenBurns;
    String Test;

    private AnimationDrawable myAnimationDrawable1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        fourSquare = (ImageView) findViewById(R.id.fourSquare);
        fourSquare.setVisibility(View.VISIBLE);

        mKenBurns = (KenBurnsView) findViewById(R.id.ken_burns_images);
        mKenBurns.setImageResource(R.drawable.world_background);
        fourSquareAnimation();
        img_scholaships_app_logo = (ImageView) findViewById(R.id.img_scholaships_app_logo);
        textView_uae = (TextView) findViewById(R.id.textView_uae);
        textView_portal = (TextView) findViewById(R.id.textView_portal);
        YoYo.with(Techniques.ZoomInDown).playOn(img_scholaships_app_logo);
//        animation2();
        animation3();
        animation4();

//        setSupportActionBar(toolbar);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);


    }


    public void fourSquareAnimation() {
        myAnimationDrawable1 = (AnimationDrawable) fourSquare.getDrawable();
        myAnimationDrawable1.start();
    }


    private void animation2() {
        img_scholaships_app_logo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.tranlate_top_to_center);
        img_scholaships_app_logo.startAnimation(anim);
    }

    private void animation3() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(textView_uae, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1700);
        alphaAnimation.setDuration(500);
        alphaAnimation.start();
    }

    private void animation4() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(textView_portal, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1700);
        alphaAnimation.setDuration(500);
        alphaAnimation.start();
    }


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        Target viewTarget = new ViewTarget(R.id.fab, this);
//
//
//        new ShowcaseView.Builder(this)
//                .setTarget(viewTarget)
//                .setContentTitle("ShowcaseView")
//                .setContentText("This is highlighting the Home button")
//                .hideOnTouchOutside()
//                .build();
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_splash, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
