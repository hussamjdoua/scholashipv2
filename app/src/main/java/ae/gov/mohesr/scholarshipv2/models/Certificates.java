package ae.gov.mohesr.scholarshipv2.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hussamj on 11/23/15.
 */

@Table(name = "Certificates")
public class Certificates extends Model {

    @Column(name = "ID")
    public String ID;

    @Column(name = "MinValueListen")
    public String MinValueListen;

    @Column(name = "MaxValueListen")
    public String MaxValueListen;

    @Column(name = "MinValueRead")
    public String MinValueRead;


    @Column(name = "MaxValueRead")
    public String MaxValueRead;

    @Column(name = "MinValueWrite")
    public String MinValueWrite;

    @Column(name = "MaxValueWrite")
    public String MaxValueWrite;


    @Column(name = "MinValueConversation")
    public String MinValueConversation;


    @Column(name = "MaxValueConversation")
    public String MaxValueConversation;

    @Column(name = "CertificateName")
    public String CertificateName;


    @Column(name = "DefultValueListen")
    public String DefultValueListen;

    @Column(name = "DefultValueRead")
    public String DefultValueRead;

    @Column(name = "DefultValueWrite")
    public String DefultValueWrite;


    @Column(name = "DefultValueConversation")
    public String DefultValueConversation;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMinValueListen() {
        return MinValueListen;
    }

    public void setMinValueListen(String minValueListen) {
        MinValueListen = minValueListen;
    }

    public String getMaxValueListen() {
        return MaxValueListen;
    }

    public void setMaxValueListen(String maxValueListen) {
        MaxValueListen = maxValueListen;
    }

    public String getMinValueRead() {
        return MinValueRead;
    }

    public void setMinValueRead(String minValueRead) {
        MinValueRead = minValueRead;
    }

    public String getMaxValueRead() {
        return MaxValueRead;
    }

    public void setMaxValueRead(String maxValueRead) {
        MaxValueRead = maxValueRead;
    }

    public String getMinValueWrite() {
        return MinValueWrite;
    }

    public void setMinValueWrite(String minValueWrite) {
        MinValueWrite = minValueWrite;
    }

    public String getMaxValueWrite() {
        return MaxValueWrite;
    }

    public void setMaxValueWrite(String maxValueWrite) {
        MaxValueWrite = maxValueWrite;
    }

    public String getMinValueConversation() {
        return MinValueConversation;
    }

    public void setMinValueConversation(String minValueConversation) {
        MinValueConversation = minValueConversation;
    }

    public String getMaxValueConversation() {
        return MaxValueConversation;
    }

    public void setMaxValueConversation(String maxValueConversation) {
        MaxValueConversation = maxValueConversation;
    }

    public String getCertificateName() {
        return CertificateName;
    }

    public void setCertificateName(String certificateName) {
        CertificateName = certificateName;
    }

    public String getDefultValueListen() {
        return DefultValueListen;
    }

    public void setDefultValueListen(String defultValueListen) {
        DefultValueListen = defultValueListen;
    }

    public String getDefultValueRead() {
        return DefultValueRead;
    }

    public void setDefultValueRead(String defultValueRead) {
        DefultValueRead = defultValueRead;
    }

    public String getDefultValueWrite() {
        return DefultValueWrite;
    }

    public void setDefultValueWrite(String defultValueWrite) {
        DefultValueWrite = defultValueWrite;
    }

    public String getDefultValueConversation() {
        return DefultValueConversation;
    }

    public void setDefultValueConversation(String defultValueConversation) {
        DefultValueConversation = defultValueConversation;
    }
}
