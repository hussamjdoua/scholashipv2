package ae.gov.mohesr.scholarshipv2.main;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextPaint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import ae.gov.mohesr.scholarshipv2.R;

/**
 * Created by hussamj on 11/22/15.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    EditText input_username, input_password;
    Button btn_login;
    TextView label_regiter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        input_username = (EditText) findViewById(R.id.input_password);
        input_password = (EditText) findViewById(R.id.input_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        label_regiter = (TextView) findViewById(R.id.label_regiter);
        label_regiter.setOnClickListener(this);

        Target viewTarget = new ViewTarget(R.id.label_regiter, this);

        TextPaint paint = new TextPaint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setShadowLayer(2f, 0f, 0f, Color.BLACK);


        paint.setColor(Color.BLACK);
        paint.setTextSize(20);

        new ShowcaseView.Builder(this)
                .setTarget(viewTarget)
                .setContentTitle("New student")
                .setContentText("here where you register")
                .setContentTitlePaint(paint)
                .setContentTextPaint(paint)

                .hideOnTouchOutside()
                .build();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.label_regiter:
                Intent intent = new Intent(MainActivity.this, RegistarionActivity.class);
                startActivity(intent);

                break;
        }


    }
}
