package ae.gov.mohesr.scholarshipv2.app.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

import com.squareup.okhttp.RequestBody;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hussamj on 9/21/15.
 */
public class AppAsync extends AsyncTask<Void, Void, String> {

    ProgressDialog progress;
    SweetAlertDialog pDialog;
    private OnConnectionCompleted listener;
    private String Url;
    private Context context;
    private int connectionTag;
    ConnectionType connectionType;
    String json;
    Exception ex;
    RequestBody formBody;

    public AppAsync(OnConnectionCompleted listener, String url, Context context, int connectionTag, ConnectionType connectionType, RequestBody requestBody) {
        this.listener = listener;
        Url = url;
        this.context = context;
        this.connectionTag = connectionTag;
        this.connectionType = connectionType;
        formBody = requestBody;

    }


    public ProgressDialog getProgress() {
        return progress;
    }

    public void setProgress(ProgressDialog progress) {
        this.progress = progress;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.getProgressHelper().setBarWidth(10);
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

//        progress = new ProgressDialog(context,
//                ProgressDialog.THEME_DEVICE_DEFAULT_DARK);
//        progress.setMessage("Loading...");
//        progress.setCancelable(false);
//        progress.setCanceledOnTouchOutside(false);
//        progress.show();

    }

    @Override
    protected String doInBackground(Void... params) {

        try {
            DefaultConnections conn = new DefaultConnections();
            String result = null;


            switch (connectionType) {
                case GetMethod:
                    result = conn.doGetRequest(Url);
                    break;
                case PostMethod:
                    result = conn.doPostRequest(Url, formBody);
                    break;

                default:
                    break;
            }

            if (result == null)
                ex = conn.getException();


            return result;


        } catch (Exception e) {
            // TODO: handle exception
            ex = e;
            return null;

        }
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//            progress.dismiss();
//        pDialog.getProgressHelper().getProgressWh;
        pDialog.dismiss();


        if (listener != null) {

            if (result == null)
                listener.OnConnectionFailed(ex, connectionTag);
            else
                listener.OnConnectionSuccess(result, connectionTag);

        }


    }
}
