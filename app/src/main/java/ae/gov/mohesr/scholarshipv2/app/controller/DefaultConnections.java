package ae.gov.mohesr.scholarshipv2.app.controller;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by hussamj on 9/21/15.
 */
public class DefaultConnections {

    protected static DefaultConnections instance = null;

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();

    public static synchronized DefaultConnections getInstance() {
        if (instance == null) {
            instance = new DefaultConnections();
        }
        return instance;
    }


    public Exception exception = null;

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }


    //post method

    String doPostRequest(String url, RequestBody body) {
//        RequestBody body = RequestBody.create(JSON, json);
//        RequestBody formBody = new FormEncodingBuilder()
//                .add("Languge", "1")
//                .build();
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            this.exception = e;
            return null;
        }

    }

    //get method
    String doGetRequest(String url) {
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            this.exception = e;

            return null;
        }
    }

}
