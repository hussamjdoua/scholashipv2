package ae.gov.mohesr.scholarshipv2.app.controller;

public interface OnConnectionCompleted {


    public void OnConnectionSuccess(String result, int ConnectionTag);

    public void OnConnectionFailed(Exception ex, int ConnectionTag);

    public void OnInternetDisabled();


}
