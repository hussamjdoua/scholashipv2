package ae.gov.mohesr.scholarshipv2.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by hussamj on 11/23/15.
 */
@Table(name = "LookUps")
public class LookUpsModel extends Model {

    @Column(name = "ID")
    public String ID;

    @Column(name = "DescAr")
    public String DescAr;


    @Column(name = "DescEn")
    public String DescEn;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getDescAr() {
        return DescAr;
    }

    public void setDescAr(String descAr) {
        DescAr = descAr;
    }

    public String getDescEn() {
        return DescEn;
    }

    public void setDescEn(String descEn) {
        DescEn = descEn;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getUniversityId() {
        return UniversityId;
    }

    public void setUniversityId(String universityId) {
        UniversityId = universityId;
    }

    public String getMajorId() {
        return MajorId;
    }

    public void setMajorId(String majorId) {
        MajorId = majorId;
    }

    public boolean isHasAttach() {
        return hasAttach;
    }

    public void setHasAttach(boolean hasAttach) {
        this.hasAttach = hasAttach;
    }

    @Column(name = "CountryId")
    public String CountryId;

    @Column(name = "UniversityId")
    public String UniversityId;

    @Column(name = "MajorId")
    public String MajorId;

    @Column(name = "hasAttach")
    public boolean hasAttach;

    @Column(name = "lookUpType")
    public String lookUpType;

    @Column(name = "CountrySet")
    public String CountrySet;

    public String getCountrySet() {
        return CountrySet;
    }

    public void setCountrySet(String countrySet) {
        CountrySet = countrySet;
    }

    public String getLookUpType() {
        return lookUpType;
    }

    public void setLookUpType(String lookUpType) {
        this.lookUpType = lookUpType;
    }
}
