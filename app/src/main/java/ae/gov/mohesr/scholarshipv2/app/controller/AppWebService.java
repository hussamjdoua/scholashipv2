package ae.gov.mohesr.scholarshipv2.app.controller;

import android.content.Context;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

/**
 * Created by hussamj on 9/22/15.
 */
public class AppWebService {

    protected static AppWebService instance = null;

    public static synchronized AppWebService getInstance() {
        if (instance == null) {
            instance = new AppWebService();
        }
        return instance;
    }

    public RequestBody getAllEmarats(Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .build();

        return formBody;
    }

    public RequestBody getAllJobs(Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .build();

        return formBody;
    }

    public RequestBody getAllJobsDetail(String JobID, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("JobID", JobID)
                .build();

        return formBody;
    }

    public RequestBody getAllInstitions(String EmarahId, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("EmaraID", EmarahId)
                .build();

        return formBody;
    }

    public RequestBody getAllDegrees(String institueID, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("FoundationID", institueID)
                .build();

        return formBody;
    }

    public RequestBody getAllMajors(String institueID, String DegreeID, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("FoundationID", institueID)
                .add("DegreeID", DegreeID)

                .build();

        return formBody;
    }

    public RequestBody getUniversityDetails(String FoundationID, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Language", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("FoundationID", FoundationID)

                .build();
        return formBody;
    }


    public RequestBody getSpecialtyDetails(String FoundationID, String DegreeID, String SpeciID, Context context) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("Languge", AppSharedPrefrences.getInstance().getSharedPreferances(context, AppConstants.KEY_LOCAL_LANGUAGE_API))
                .add("FoundationID", FoundationID)
                .add("DegreeID", DegreeID)
                .add("SpeciID", SpeciID)

                .build();

        return formBody;
    }


}
