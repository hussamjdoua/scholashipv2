package ae.gov.mohesr.scholarshipv2.app.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by hussamj on 9/22/15.
 */
public class AppInternetConnections {
    protected static AppInternetConnections instance = null;

    public static synchronized AppInternetConnections getInstance() {
        if (instance == null) {
            instance = new AppInternetConnections();
        }
        return instance;
    }


    public final boolean isInternetOn(Context cox) {
        boolean result = false;
        @SuppressWarnings("static-access")
        ConnectivityManager con_manager = (ConnectivityManager) cox
                .getSystemService(cox.CONNECTIVITY_SERVICE);
        // ARE WE CONNECTED TO THE NET
        if (con_manager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                || con_manager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            result = true;
        } else if (con_manager.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                || con_manager.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {


            Toast.makeText(cox, "no internet",
                    Toast.LENGTH_LONG).show();
            result = false;
        }
        return result;
    }
}
