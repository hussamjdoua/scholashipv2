package ae.gov.mohesr.scholarshipv2.registration;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.viewpagerindicator.IconPagerAdapter;

import ae.gov.mohesr.scholarshipv2.registration.fragments.RegistrationPersonalInfoFragment;

/**
 * Created by hussamj on 12/1/15.
 */
public class RegistrationTabsAdapter extends FragmentPagerAdapter implements IconPagerAdapter {

    public RegistrationTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new RegistrationPersonalInfoFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getIconResId(int index) {
        return 0;
    }

    @Override
    public int getCount() {
        return 0;
    }
}
