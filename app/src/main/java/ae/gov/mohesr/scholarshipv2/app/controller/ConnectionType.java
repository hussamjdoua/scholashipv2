package ae.gov.mohesr.scholarshipv2.app.controller;

/**
 * Created by hussamj on 9/21/15.
 */
public enum ConnectionType {
    PostMethod, GetMethod;
}
