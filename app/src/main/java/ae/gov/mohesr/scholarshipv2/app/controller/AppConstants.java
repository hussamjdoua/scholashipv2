package ae.gov.mohesr.scholarshipv2.app.controller;

/**
 * Created by hussamj on 9/22/15.
 */
public class AppConstants {


    public static final String DOMAIN_NAME = "http://79.134.147.14/Daleel.WebService/MobileDaleel.asmx/";

    public static final String API_ALL_EMIRATES = DOMAIN_NAME + "Daleel_GetAllEmaras";

    public static final String API_ALL_JOBS = DOMAIN_NAME + "Daleel_GetAllJobs";

    public static final String API_JOB_DETAIL = DOMAIN_NAME + "Daleel_GetDetailJob";

    public static final String API_ALL_INSTITUTIONS = DOMAIN_NAME + "Daleel_GetEducationalInstitutionByEmaraID";
    public static final String API_ALL_DEGREES = DOMAIN_NAME + "Daleel_GetDegreeByFoundationID";
    public static final String API_ALL_MAJORS = DOMAIN_NAME + "Daleel_GetSpecialization";
    public static final String KEY_PLACMENT_TEST = "KEY_PLACMENT_TEST";

    public static final String API_GetDetailFoundation = DOMAIN_NAME + "Daleel_GetDetailFoundation";
    public static final String API_GetSpecializationInfo = DOMAIN_NAME + "Daleel_GetSpecializationInfo";


    public static final String JSON_ARRAY_EMIRATES = "listEmara";
    public static final String JSON_ARRAY_INSTITUTIONS = "listEducationalInstitution";
    public static final String JSON_ARRAY_DEGREES = "listDegree";
    public static final String JSON_ARRAY_MAJORS = "listSpecialization";
    public static final String JSON_ARRAY_JOBS = "listJobs";
    public static final String JSON_ARRAY_JOBS_DETAIL = "listDetailJob";


    //Connection tags
    public static final int TAG_EMIRATES = 1;
    public static final int TAG_ORGANIZATION = 2;
    public static final int TAG_DEGREE = 3;
    public static final int TAG_MAJOR = 4;
    public static final int TAG_ALL_JOBS = 5;
    public static final int TAG_JOB_DETAIL = 6;


    // Amjad Constants

    public static final String KEY_EMARA_ID = "KEY_EMARA_ID";
    public static final String KEY_ORGNIZATION_ID = "KEY_ORGNIZATION_ID";
    public static final String KEY_MAJOR_ID = "KEY_MAJOR_ID";
    public static final String KEY_DEGREE_ID = "KEY_DEGREE_ID";
    public static final String KEY_UNIVERSITY = "KEY_UNIVERSITY";
    public static final String KEY_LOCAL_LANGUAGE = "KEY_LOCAL_LANGUAGE";
    public static final String KEY_LOCAL_LANGUAGE_API = "KEY_LOCAL_LANGUAGE_API";
    public static final String KEY_API_ARABIC = "1";
    public static final String KEY_API_ENGLISH = "2";
    public static final String KEY_LANGUAGE_ARABIC = "ar";
    public static final String KEY_LANGUAGE_ENGLISH = "en-us";
    public static final String KEY_Description = "KEY_Description";


}
